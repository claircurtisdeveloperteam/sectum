<?php
/** 
 * A WordPress fő konfigurációs állománya
 *
 * Ebben a fájlban a következő beállításokat lehet megtenni: MySQL beállítások
 * tábla előtagok, titkos kulcsok, a WordPress nyelve, és ABSPATH.
 * További információ a fájl lehetséges opcióiról angolul itt található:
 * {@link http://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php} 
 *  A MySQL beállításokat a szolgáltatónktól kell kérni.
 *
 * Ebből a fájlból készül el a telepítési folyamat közben a wp-config.php
 * állomány. Nem kötelező a webes telepítés használata, elegendő átnevezni 
 * "wp-config.php" névre, és kitölteni az értékeket.
 *
 * @package WordPress
 */

// ** MySQL beállítások - Ezeket a szolgálatótól lehet beszerezni ** //
/** Adatbázis neve */
define('DB_NAME', 'sectum');

/** MySQL felhasználónév */
define('DB_USER', 'root');

/** MySQL jelszó. */
define('DB_PASSWORD', 'root');

/** MySQL  kiszolgáló neve */
define('DB_HOST', 'localhost');

/** Az adatbázis karakter kódolása */
define('DB_CHARSET', 'utf8mb4');

/** Az adatbázis egybevetése */
define('DB_COLLATE', '');

/**#@+
 * Bejelentkezést tikosító kulcsok
 *
 * Változtassuk meg a lenti konstansok értékét egy-egy tetszóleges mondatra.
 * Generálhatunk is ilyen kulcsokat a {@link http://api.wordpress.org/secret-key/1.1/ WordPress.org titkos kulcs szolgáltatásával}
 * Ezeknek a kulcsoknak a módosításával bármikor kiléptethető az összes bejelentkezett felhasználó az oldalról. 
 *
 * @since 2.6.0
 */
define('AUTH_KEY', ',2Yn)[iZ]3{nZB:~m/VlBl> ad}^Kkx7.+.fj{wWbnp5FSnY4|C<CKmu/IahtM-^');
define('SECURE_AUTH_KEY', '8`y>N<_B|5qXg-tnL$Uzm_v>l >vUw*!5`;l$VuS$6>T6&?^0SBY@.s7)oPU7Ccy');
define('LOGGED_IN_KEY', 'a^?)P2?5PU<Pn*Bu3Q0l-hRzQ]^YG9@<m3f7p*d};!sLBT&;I-9@!O]+hEyj1TV_');
define('NONCE_KEY', '`oG4~+{EmC2gh%8Un(Of7OX7MvxV$v$XyzGrj]R)as?wzfl1F)K5?Ms*m5)=L9>g');
define('AUTH_SALT',        '<`XfQzYvJTp4ZmLJ3rHBy21#194t4E6)ySM1cP,[8y85*+ncLY;bV%X-waNS1/$r');
define('SECURE_AUTH_SALT', 'z$iiJ?uOkFVyK<kqw `{am`|?_2UfIVi4Y/Xk#9hT5}zxHZU2#=a7-gF.aK2]@Y[');
define('LOGGED_IN_SALT',   'H[pb)uK|1*D/7e]c[qk=t+&UQ?&AZmz$NKpl}y6^FRN/_F=?BCeTh?N~}N8c&!@K');
define('NONCE_SALT',       'X)u0}N.x2lFxEKR1&ZzR4^:_,1Q&&N>_)mo!-f9,wK~sw-wAZIw;zii!LzhnHZkZ');

/**#@-*/

/**
 * WordPress-adatbázis tábla előtag.
 *
 * Több blogot is telepíthetünk egy adatbázisba, ha valamennyinek egyedi
 * előtagot adunk. Csak számokat, betűket és alulvonásokat adhatunk meg.
 */
$table_prefix  = 'wp_';

/**
 * Fejlesztőknek: WordPress hibakereső mód.
 *
 * Engedélyezzük ezt a megjegyzések megjelenítéséhez a fejlesztés során. 
 * Erősen ajánlott, hogy a bővítmény- és sablonfejlesztők használják a WP_DEBUG
 * konstansot.
 */
define('WP_DEBUG', false);

/* Ennyi volt, kellemes blogolást! */

/** A WordPress könyvtár abszolút elérési útja. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Betöltjük a WordPress változókat és szükséges fájlokat. */
require_once(ABSPATH . 'wp-settings.php');
